﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductsAPI.Repositories;
using System;
using System.Linq;

namespace ProductsAPI.Tests.Unit
{
    [TestClass]
    public class BogusProductsRepositoryTests
    {
        [TestMethod]
        public void WhenDeletingExistingProduct_ThenReturnsTrue()
        {
            var deletingProductId = 1;
            var productsCount = 5;
            var repository = new BogusProductsRepository(productsCount);

            var isDeleted = repository.Delete(deletingProductId);
            Assert.IsTrue(isDeleted);
        }

        [TestMethod]
        public void WhenDeletingNonExistingProduct_ThenReturnsFalse()
        {
            var deletingProductId = 10;
            var productsCount = 5;
            var repository = new BogusProductsRepository(productsCount);

            var isDeleted = repository.Delete(deletingProductId);
            Assert.IsFalse(isDeleted);
        }

        [TestMethod]
        public void WhenDeletingProductFromEmptyCollection_ThenReturnsFalse()
        {
            var deletingProductId = 1;
            var productsCount = 0;
            var repository = new BogusProductsRepository(productsCount);

            var isDeleted = repository.Delete(deletingProductId);
            Assert.IsFalse(isDeleted);
        }

        [TestMethod]
        public void WhenProductsCountEqualsZero_ThenReturnsEmptyProductsCollection()
        {
            var productsCount = 0;
            var repository = new BogusProductsRepository(productsCount);
            var products = repository.GetAll();

            Assert.AreEqual(productsCount, products.Count());
        }

        [TestMethod]
        public void WhenProductsCountIsNegative_ThenArgumentOutOfRangeExceptionIsThrown()
        {
            var productsCount = -5;
            Action action = () => new BogusProductsRepository(productsCount);

            Assert.ThrowsException<ArgumentOutOfRangeException>(action);
        }

        [TestMethod]
        public void WhenProductsCountIsPositive_ThenReturnsCollectionWithTheSameNumberOfProducts()
        {
            var productsCount = 5;
            var repository = new BogusProductsRepository(productsCount);
            var products = repository.GetAll();

            Assert.AreEqual(productsCount, products.Count());
        }
    }
}

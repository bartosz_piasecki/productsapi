﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Unity;
using Unity.Exceptions;

namespace ProductsAPI.Resolvers
{
    public class UnityResolver : IDependencyResolver
    {
        private IUnityContainer container;

        public UnityResolver(IUnityContainer container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            IEnumerable<object> services = null;

            try
            {
                services = container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException)
            {
                services = new List<object>();
            }

            return services;
        }

        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            var resolver = new UnityResolver(child);

            return resolver;
        }

        public object GetService(Type serviceType)
        {
            object service = null;

            try
            {
                service = container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            { }

            return service;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                container.Dispose();
                container = null;
            }
        }
    }
}
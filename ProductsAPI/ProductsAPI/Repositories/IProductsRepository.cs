﻿using ProductsAPI.Models;
using System.Collections.Generic;

namespace ProductsAPI.Repositories
{
    public interface IProductsRepository
    {
        IEnumerable<Product> GetAll();
        bool Delete(int id);
    }
}
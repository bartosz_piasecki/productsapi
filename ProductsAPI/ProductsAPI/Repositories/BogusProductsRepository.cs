﻿using Bogus;
using ProductsAPI.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public class BogusProductsRepository : IProductsRepository
    {
        private const int RandomizerSeed = 123456789;

        private readonly ConcurrentDictionary<int, Product> Products;

        public BogusProductsRepository(int productsCount)
        {
            Randomizer.Seed = new Random(RandomizerSeed);
            Products = new ConcurrentDictionary<int, Product>(CreateProducts(productsCount));
        }

        public IEnumerable<Product> GetAll() =>
            Products.Values;

        public bool Delete(int id)
        {
            var isDeleted = Products.TryRemove(id, out Product product);
            return isDeleted;
        }

        private IEnumerable<KeyValuePair<int, Product>> CreateProducts(int count)
        {
            var productsId = 0;
            var productsFaker = new Faker<Product>().
                RuleFor(x => x.Id, x => ++productsId).
                RuleFor(x => x.Name, x => x.Commerce.ProductName()).
                RuleFor(x => x.Description, (x, y) => $"{x.Commerce.ProductMaterial()} {x.Commerce.Color()} {y.Name.ToLower()}.").
                RuleFor(x => x.Price, x => decimal.Parse(x.Commerce.Price())).
                RuleFor(x => x.PictureUrl, x => x.Image.Image());

            var products = productsFaker.Generate(count).Select(x => new KeyValuePair<int, Product>(x.Id, x));
            return products;
        }
    }
}
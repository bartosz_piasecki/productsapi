﻿using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace ProductsAPI.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly IProductsRepository repository;

        public ProductsController(IProductsRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<Product> Get() =>
            repository.GetAll();

        public IHttpActionResult Delete(int id)
        {
            var isDeleted = repository.Delete(id);

            if (!isDeleted)
                return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
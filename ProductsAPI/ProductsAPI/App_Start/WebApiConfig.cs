﻿using ProductsAPI.Repositories;
using ProductsAPI.Resolvers;
using System;
using System.Net.Http.Formatting;
using System.Web.Http;
using Unity;

namespace ProductsAPI
{
    public static class WebApiConfig
    {
        private const int DefaultProductsCount = 20;

        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterInstance(typeof(IProductsRepository), new BogusProductsRepository(DefaultProductsCount));

            config.DependencyResolver = new UnityResolver(container);
            config.Formatters.JsonFormatter.MediaTypeMappings.
               Add(new RequestHeaderMapping("Accept", "text/html", StringComparison.InvariantCultureIgnoreCase, true, "application/json"));

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

﻿namespace ProductsAPI.Models
{
    public class Product
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public string PictureUrl { get; set; }
        public decimal Price { get; set; }
        public int Id { get; set; }
    }
}